# Algorithms for Embedded Engineers
Authors: Alex Mault, TBA


## Overview
This course is to be taken by undergraduates in their third or fourth(?) year to expose 
students to algorithms generally used or unique to the embedded systems world.

This topic has been identified as unique and needed by industry professionals from
companies such as Western Digital, (others?). By providing this knowledge and skill set
to soon-to-be professionals, individuals will be better prepared to obtain and accel 
in the world of embedded systems engineering.

Students will be hands-on, implementing, testing, debugging, and delivering labs 
utilizing algorithms taught throughout the semester. The course will be taught using the
`EFM32PG12 Pearl Gecko ARM® Cortex®-M4 MCU 32-Bit Embedded Evaluation Board`.
Students are welcome to participate with other boards but NO SUPPORT WILL BE PROVIDED
for board unique issues.  


## Incoming Skill-Sets
Students entering this class are expected to meet the following prerequisites
- Proficient in C
- Strong understanding of Microcontrollers
  - Understanding Registers and reading datasheets
  - On-Chip Peripherals
- Use of an RTOS and basic Scheduling Logic
- Basic Circuit Design / Breadboarding
- Calculus 3(?)

## Outcome Targets
- Understand the definition of an Algorithm
- Understand tradeoffs between speed, memory usage, storage size 
- Implement Algorithms from scratch as well as using existing libraries
- Understand WHEN to use libraries, write-yourself, or use Embedded peripherals
- Properly apply Algorithmic logic to incoming data from other systems
- A working knowledge of commonly used Algorithms
- A basic knowledge and summary of cryptography in practice (hint: NEVER ROLL YOUR OWN!)

## Outline 
1. Defining an "Algorithm"  (week 1)
  - What is it?
  - How do you measure performance?
    - speed (Big-O)
    - Memory Usage
  - What to think about when reaching into your "Algorithm tool belt"

2. What's different about algorithms in an Embedded System? (week 1)
  - Significantly more constrained resources
  - Unique perferierals (may help!)

3. Searching, Sorting, Structuring, recursion (week 2-5)
  - Linear Search
  - Trees / Binary Search 
  - Graphs
  - Recursive implementation of some 
  ( For each, build a strong "use case" in students mind, associated with performance, and reasons to use each)

4. PID (week 5-6)
  - A lab in which students must implement software that takes in values from test-jig via (UART? SPI? I2C?)
    performs PID on the input, and responds with a "correction" value. 

5. Error Correction (week 7-8)
  - A lab in which students must implement software that takes values in from test-jig via (UART? SPI? I2C?)
    and performs error correction, displaying the corrected message on the screen and replying with the corrected result.
    (JIG will purposefully send bad-bytes that are correctable in-order to simulate flipped bits)

6. Random Number Generation (week 9-10)
  - Significant security flaws have come from companies in-correctly
    generating random-numbers. (Sony's PS3 for example). 
  - where are RNG's used? What are practical applications
  - Learn about "SEEDING" a random number generator


7. CRC / Hashes (SHA, MD5)  (10-13)
  - Example of a complex Algorithm used widely
  - What is hashing? WHY do it? Where is it used in the world.
  - Initially implement in software, test performance
  - Gecko has hardware modules, switch to using them. Test performance.
  






